from numba import njit


@njit()
def verlet1(step, accelerations, positions, velocities):
    """Saves current acceleration array and then iterates position using velocity-verlet"""
    a_prev = accelerations.copy()
    positions += velocities * step + 0.5 * accelerations * step**2
    return a_prev, positions


@njit()
def verlet2(step, velocities, accelerations, a_prev):
    """Uses old and current acceleration array to update the velocity array"""
    velocities += 0.5 * (a_prev + accelerations) * step
    return velocities


@njit()
def ncromer(step, positions, velocities, accelerations):
    """Returns updated position and velocity arrays based on the current acceleration of each body."""
    velocities += step * accelerations
    positions += step * velocities
    return positions, velocities
