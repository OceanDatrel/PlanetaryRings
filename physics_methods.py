from numba import njit
import numpy as np


@njit()
def collision_and_grav_acc(grav, accelerations, positions, masses, velocities):
    """Calculates the gravitational acceleration on every body due to each other body. Resolves elastic collisions
    between mass packets."""
    for i in enumerate(accelerations):
        acc = np.zeros(2)
        for j in enumerate(accelerations):
            if i[0] == j[0]:
                # Saves an extra calculation for an inherently zero acceleration
                continue
            sep_vec = positions[j[0]] - positions[i[0]]
            # collisions are included in this method so we can reuse this vector without recalculation
            sep_vec_mod = (sep_vec[0] ** 2 + sep_vec[1] ** 2) ** 0.5
            if sep_vec_mod <= 1e-15:
                continue
            if j[0] != 0 and i[0] != 0 and i[0] < j[0] and 2 >= sep_vec_mod:
                # only considers collisions between packets of 1m radius
                print("Collision detected.")
                v_diff = velocities[i[0]] - velocities[j[0]]
                v_diff_dot_sep_vec = (v_diff[0] * sep_vec[0] + v_diff[1] * sep_vec[1])
                delta = sep_vec * (v_diff_dot_sep_vec / sep_vec_mod ** 2)
                velocities[i[0]] += -delta
                velocities[j[0]] += delta
            # calculate gravitational attraction on i with direction toward the body j
            acc += (grav * masses[j[0]] * sep_vec) / (sep_vec_mod ** 3)
        accelerations[i[0]] = acc
    return accelerations, velocities


@njit()
def tot_momentum(masses, velocities):
    """Returns the total scalar linear momentum of all bodies in the system."""
    total = 0
    for i in enumerate(masses):
        total += (velocities[i[0]][0] ** 2 + velocities[i[0]][1] ** 2) ** 0.5 * masses[i[0]]
    return total


@njit()
def tot_energy(grav, masses, positions, velocities):
    """Returns the total energy of the system."""
    total = 0
    for i in enumerate(masses):
        total += 0.5 * masses[i[0]] * (velocities[i[0]][0] ** 2 + velocities[i[0]][1] ** 2)
        for j in enumerate(masses):
            if i[0] >= j[0]:
                # skip if evaluating self or something which has already had the energy calculated between
                continue
            sep_vec = positions[i[0]] - positions[j[0]]
            total += -(grav * masses[i[0]] * masses[j[0]]) / ((sep_vec[0] ** 2 + sep_vec[1] ** 2) ** 0.5)
    return total
