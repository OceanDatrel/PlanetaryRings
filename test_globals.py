from system import *
from config_reader import *


def energy_and_momentum_test(system_type="pre-gen"):
    """Tests to see if the final momentum and energy of the system match their initial values after
     the default simulation."""
    if system_type == "configured":
        # read default config file
        read = data_importer("config.cfg")
        # initialise a System class from that config
        test_case = System(read[0], read[1], read[2], read[3], read[4], read[5], read[6])

    elif system_type == "pre-gen":
        # initialise a simple pre-generated stable orbital
        test_case = System(1000,
                           1,
                           np.array([1e10, 0], dtype=float),
                           np.array([[0, 0], [0, 1.4]], dtype=float),
                           np.array([[0, 0], [2.2, 0]], dtype=float),
                           "euler-cromer")
    else:
        raise ValueError("Not a recognised type of system")

    # generate initial values
    init_m = tot_momentum(test_case.masses, test_case.velocities)
    init_e = tot_energy(test_case.G, test_case.masses, test_case.positions, test_case.velocities)
    # evolve the system
    test_case.simulate()
    # generate after values
    aft_m = tot_momentum(test_case.masses, test_case.velocities)
    aft_e = tot_energy(test_case.G, test_case.masses, test_case.positions, test_case.velocities)
    # compare
    con_e = np.isclose(aft_e, init_e)
    con_m = np.isclose(aft_m, init_m)
    # output based on the tests
    if con_e and con_m:
        return "Success"
    elif con_e and not con_m:
        return "Failure: Momentum is\n{0}\nbut should be\n{1}".format(aft_m, init_m)
    elif con_m and not con_e:
        return "Failure: Energy is\n{0}\nbut should be\n{1}".format(aft_e, init_e)
    else:
        return "Failure: Energy is\n{0}\nbut should be\n{1} and momentum is\n{2}\nbut should be\n{3}".format(aft_e,
                                                                                                             init_e,
                                                                                                             aft_m,
                                                                                                             init_m)


print(energy_and_momentum_test("configured"))
