# PlanetaryRings
Project for Lancaster University Physics to simulate planetary discs and the motion of particles therein.
An outline of the functions of the different files in this project is provided below.

* config_reader.py - Produces the parameters needed for the initialisation of a System class modelling a masssive body
being orbited by a large amount of smaller mass packets in a disc.
* default.cfg - A default/template configuration file for config_reader.py. The default configuration is intended to
approximate the Saturn planetary belt system in terms of mass and belt size. All units are in SI.
* frontend.py - Allows the user to input the name of their intended config file, and then initialises and times the
evolution of a System class based on the configured parameters.
* integrators.py - The functions for the Euler-Cromer and velocity-Verlet numerical integration methods are
stored here.
* physics_methods.py - The functions for the physical interactions of the system are stored here. This includes
Newtonian gravitation, simple elastic collisions, the calculation of total energy, and determination of total linear
momentum
* system.py - The class file for the System class. This is responsible for the calling of the static functions where
necessary and the creation of datafiles for energy, momentum, and body positions.
* test_collision - Performs one of three collision scenarios for the unit testing of the collision model.
* test_globals - Tests the conservation of momentum and energy.
* test_orbit - Ensures that stable orbits remain so over a long timescale by measuring the difference in position after
integer multiples of an orbital period.
