from system import *


def orbit_test(integrator, step, n_orbits):
    """Tests to see if a massless body orbiting a body of fixed mass will return to its position after
    a given number of orbital periods(of 840001s) have passed."""
    central_mass = 6e26
    r = (840001 * ((constants.G * central_mass) ** 0.5) / (2 * np.pi)) ** (2 / 3)
    v = (central_mass*constants.G/r)**0.5
    # above equations simply derived from circular orbits

    test_case = System(840001*n_orbits,
                       step,
                       np.array([central_mass, 0], dtype=float),
                       np.array([[0, 0], [0, r]], dtype=float),
                       np.array([[0, 0], [v, 0]], dtype=float),
                       integrator)

    expected_result = np.array([[0, 0], [0, r]], dtype=float)

    test_case.simulate()

    if np.allclose(test_case.positions, expected_result, atol=r*1e-6):  # test the orbital radius error at 1ppm
        return "Success"

    else:
        return "Failure: positions are\n{0}\nbut should be\n{1}".format(test_case.positions, expected_result)


print(orbit_test("euler-cromer", 2, 10))
