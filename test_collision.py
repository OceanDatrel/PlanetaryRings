from system import *


def collision_test(collision_type="static"):
    """Tests to see if the final velocities of two particles is as expected after one of three known collision types."""
    # set out a physical scenario leading to an elastic collision with a known result
    if collision_type == "glance":
        test_case = System(2,
                           1,
                           np.array([0, 0, 0]),
                           np.array([[0, 0], [-10, 0], [6, 1]], dtype=float),
                           np.array([[0, 0], [10, 0], [-5, 0]], dtype=float),
                           "euler-cromer")

        expected_result = np.array([[0, 0], [2.5, -7.5], [2.5, 7.5]], dtype=float)

    elif collision_type == "static":
        test_case = System(2,
                           1,
                           np.array([0, 0, 0]),
                           np.array([[0, 0], [0, 0], [2, 0]], dtype=float),
                           np.array([[0, 0], [0, 0], [0, 0]], dtype=float),
                           "euler-cromer")

        expected_result = np.array([[0, 0], [0, 0], [0, 0]], dtype=float)

    elif collision_type == "head_on":
        test_case = System(2,
                           1,
                           np.array([0, 0, 0]),
                           np.array([[0, 0], [0, 0], [2, 0]], dtype=float),
                           np.array([[0, 0], [1, 0], [-1, 0]], dtype=float),
                           "euler-cromer")

        expected_result = np.array([[0, 0], [-1, 0], [1, 0]], dtype=float)

    else:
        raise ValueError("Not a recognised type of collision")
    # evolve the system
    test_case.simulate()
    # test if the velocity produced by the system is close enough to the expected value
    if np.allclose(test_case.velocities, expected_result):
        return "Success"

    else:
        return "Failure: velocities are\n{0}\nbut should be\n{1}".format(test_case.velocities, expected_result)


print(collision_test("static"))
