from scipy import constants
from integrators import *
from physics_methods import *


class System:
    """
    Handles a system of gravitationally attracting bodies defined in numpy arrays.

        Parameters:
            time: The duration for which the simulation should run, in seconds.\n
            step: The time steps at which the numerical integration should take place, in seconds.\n
            masses: nx1 Array describing the mass of each body to be modelled. The first entry should describe the
            central mass of the system. \n
            positions: nx2 array describing the initial positions of each body in the system. The first entry should
            describe the central mass of the system. \n
            velocities: nx2 array describing the initial velocities of each body in the system. The first entry should
            describe the central mass of the system. \n
            method: The numerical integration method that should be used for the simulation: accepts velocity-verlet or
            euler-cromer.\n
            output: What properties of the system to export to a numpy data file: accepts all, energy, momentum, and
            position.\n
    """

    def __init__(self, time, step, masses, positions, velocities, method="velocity-verlet", output=""):
        if type(time) != int or time <= 0:
            raise ValueError("Simulation time is not valid. Please check config file")
        else:
            self.duration = time
        if type(step) != int or step <= 0:
            raise ValueError("Timestep is not valid. Please check config file")
        else:
            self.step = step
        self.masses = masses.copy()
        self.positions = positions.copy()
        self.velocities = velocities.copy()
        self.accelerations = np.zeros_like(self.positions)
        if method == "velocity-verlet" or method == "euler-cromer":
            self.numerical_method = method.lower()
        else:
            raise ValueError("Please supply a known integration method. Check readme for details.")
        if output == "position" or output == "energy" or output == "momentum" or output == "all" or output == "":
            self.outputting = output
        else:
            raise ValueError("Please supply a valid quantity to output. Check readme for details.")
        self.G = constants.G
        self.time = 0

    def simulate(self):
        """Simulates the system a number of times using self.iterate() and saves data where configured to do so"""
        loops = self.duration // self.step

        if self.outputting == "position":
            data = [self.positions.copy()]
            for i in range(loops):
                self.iterate()
                self.time += self.step
                data.append(self.positions.copy())
            np.save("Position Data" + str(self.duration) + str(self.numerical_method) + str(self.step), data)

        elif self.outputting == "momentum":
            data = [[self.time, tot_momentum(self.masses, self.velocities)]]
            for i in range(loops):
                self.iterate()
                self.time += self.step
                data.append([self.time, tot_momentum(self.masses, self.velocities)])
            np.save("Momentum Data" + str(self.duration) + str(self.numerical_method) + str(self.step), data)

        elif self.outputting == "energy":
            data = [[self.time, tot_energy(self.G, self.masses, self.positions, self.velocities)]]
            for i in range(loops):
                self.iterate()
                self.time += self.step
                data.append([self.time, tot_energy(self.G, self.masses, self.positions, self.velocities)])
            np.save("Energy Data" + str(self.duration) + str(self.numerical_method) + str(self.step), data)

        elif self.outputting == "all":
            data1 = [self.positions.copy()]
            data2 = [[self.time, tot_momentum(self.masses, self.velocities)]]
            data3 = [[self.time, tot_energy(self.G, self.masses, self.positions, self.velocities)]]
            for i in range(loops):
                self.iterate()
                self.time += self.step
                data1.append(self.positions.copy())
                data2.append([self.time, tot_momentum(self.masses, self.velocities)])
                data3.append([self.time, tot_energy(self.G, self.masses, self.positions, self.velocities)])
            np.save("Position Data" + str(self.duration) + str(self.numerical_method) + str(self.step), data1)
            np.save("Momentum Data" + str(self.duration) + str(self.numerical_method) + str(self.step), data2)
            np.save("Energy Data" + str(self.duration) + str(self.numerical_method) + str(self.step), data3)

        else:
            for i in range(loops):
                self.iterate()

    def iterate(self):
        """Iterates the position and velocity of each body over the given time step
        using the chosen method of numerical integration. Calls numba-compatible static methods when necessary."""
        if self.numerical_method == "velocity-verlet":
            if self.time == 0:
                self.accelerations, self.velocities = collision_and_grav_acc(self.G,
                                                                             self.accelerations,
                                                                             self.positions,
                                                                             self.masses,
                                                                             self.velocities)
            a_prev, self.positions = verlet1(self.step, self.accelerations, self.positions, self.velocities)
            self.accelerations, self.velocities = collision_and_grav_acc(self.G,
                                                                         self.accelerations,
                                                                         self.positions,
                                                                         self.masses,
                                                                         self.velocities)
            self.velocities = verlet2(self.step, self.velocities, self.accelerations, a_prev)

        elif self.numerical_method == "euler-cromer":
            self.accelerations, self.velocities = collision_and_grav_acc(self.G,
                                                                         self.accelerations,
                                                                         self.positions,
                                                                         self.masses,
                                                                         self.velocities)
            self.positions, self.velocities = ncromer(self.step, self.positions, self.velocities, self.accelerations)
