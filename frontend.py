from config_reader import *
from system import *
import timeit


read = data_importer(input("Name of text file:"))

user_system = System(read[0], read[1], read[2], read[3], read[4], read[5], read[6])


dur = timeit.timeit("user_system.simulate()", globals=globals(), number=1)

print("Done in {0}".format(dur))
