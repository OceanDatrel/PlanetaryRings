import numpy as np
import random
from scipy import constants


def data_importer(target="default.cfg"):
    """Function which reads a target config file, and then returns a time, step, mass array, position array, velocity
     array, method, and output based on the parameters of the file. Takes the full file name as a string argument."""

    file_name = target
    grav_const = constants.G
    masses = []
    positions = [[0, 0]]
    velocities = [[0, 0]]

    with open(file_name, "r") as f:
        time = int(f.readline().split("=")[1])
        step = int(f.readline().split("=")[1])
        method = f.readline().split("=")[1].lower().strip()
        output = f.readline().split("=")[1].lower().strip()
        print("Fundamental parameters read")

        masses.append(float(f.readline().split("=")[1]))
        central_radius = float(f.readline().split("=")[1])
        print("Central mass initialised")

        inner = float(f.readline().split("=")[1])
        if inner <= central_radius:
            raise ValueError("Your ring's inner edge is inside the planet!")
        outer = float(f.readline().split("=")[1])
        if outer <= inner:
            old_inner = inner
            inner = outer
            outer = old_inner
        ring_mass = float(f.readline().split("=")[1])
        print("Ring parameters read")

        packet_number = int(f.readline().split("=")[1])
        packet_mass = ring_mass / packet_number
        for i in range(packet_number):
            position_attempt = np.array([random.uniform(inner, outer), random.uniform(inner, outer)], dtype=float)
            radius = (position_attempt[0]**2 + position_attempt[1]**2)**0.5
            while radius <= inner or radius >= outer:
                position_attempt = np.array([random.uniform(inner, outer), random.uniform(inner, outer)], dtype=float)
                radius = (position_attempt[0] ** 2 + position_attempt[1] ** 2) ** 0.5
            positions.append(position_attempt)
            velocity_mag = np.sqrt((masses[0]*grav_const)/radius)
            normal_slope = -1 * position_attempt[0]/position_attempt[1]
            velocity_dir = np.array([1, normal_slope], dtype=float)
            velocity_unit_dir = velocity_dir / np.linalg.norm(velocity_dir)
            velocities.append(velocity_mag*velocity_unit_dir)
            masses.append(packet_mass)
        print("Packets initialised")

        f.close()

    return time, step, np.array(masses), np.array(positions), np.array(velocities), method, output
